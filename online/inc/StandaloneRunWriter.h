#ifndef Hgcal10gLinkReceiver_StandaloneRunWriter_h
#define Hgcal10gLinkReceiver_StandaloneRunWriter_h

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstring>
#include <csignal>

#include <yaml-cpp/yaml.h>

#include "ShmSingleton.h"
#include "ProcessorBase.h"
#include "DataFifo.h"
#include "RecordHeader.h"
#include "RecordHalting.h"
#include "RecordResetting.h"
#include "FileWriter.h"

namespace Hgcal10gLinkReceiver {

bool continueRun;

void StandaloneRunWriterSignalHandler(int signal) {
  std::cerr << "Process " << getpid() << " received signal " 
            << signal << std::endl;
  continueRun=false;
}
  
  class StandaloneRunWriter {

  public:
    StandaloneRunWriter(unsigned l) {
      _linkNumber=l;
      _dummyReader=false;
    }

    virtual ~StandaloneRunWriter() {
    }

    void setUpAll(uint32_t fifoKey) {
      _ptrFifoShm=shmU.setup(fifoKey);
      _ptrFifoShm->coldStart();
      _ptrFifoShm->print();
    }

    virtual void setPrintEnable(bool p) {
      _printEnable=p;
    }

    virtual void setCheckEnable(bool c) {
      _checkEnable=c;
    }

    virtual void setAssertEnable(bool a) {
      _assertEnable=a;
    }

    void setDummyReader(bool b) {
      _dummyReader=b;
    }
    
    bool initializing() {
      _ptrFifoShm->initialize();
      _ptrFifoShm->print();
      return true;
    }
      
    bool configuring(uint32_t rn=time(0)) {
      _relayNumber=rn;

      if(_relayNumber<0xffffffff) {
	std::ostringstream sout;
	sout << "dat/Relay" << std::setfill('0')
	     << std::setw(10) << _relayNumber;

	std::system((std::string("mkdir ")+sout.str()).c_str());

	_fileWriter.setDirectory(sout.str());
      }
      return true;
    }

    bool reconfiguring() {
      return true;
    }
  
    bool starting(uint32_t rn=time(0)) {
      std::system("ipcs");
      _runNumber=rn;

      std::cout << "Starting flushing FIFO..." << std::endl;
      //_ptrFifoShm->flush();
      std::cout << "Flushing complete" << std::endl;

      _eventNumber=0;
	
      //_ptrFifoShm->starting();

      RecordStarting r;
      r.setHeader();
      
      YAML::Node n;
      n["RunNumber"]=_runNumber;
      std::ostringstream sout;
      sout << n;
      r.setString(sout.str());
      r.print();
      
      _fileWriter.openRun(_runNumber,_linkNumber);
      _fileWriter.write(&r);

      return true;
    }
    
    bool pausing() {
      return true;
    }
    
    bool resuming() {
      return true;
    }
    
    bool stopping() {
      RecordStopping r;
      r.setHeader();

      r.setNumberOfEvents(_eventNumber);
      r.print();
      _fileWriter.write(&r);
      _fileWriter.close();

      //_ptrFifoShm->stopping();

      return true;
    }    
  
    bool halting() {
      _ptrFifoShm->initialize();
      _ptrFifoShm->print();
      return true;
    }    

    bool ending() {
      return true;
    }    

    bool resetting() {
      _ptrFifoShm->print();

      RecordResetting r;
      if(_printEnable) r.print();
      _fileWriter.write(&r);
      _fileWriter.close();
      _ptrFifoShm->print();
      return true;
    }

    void configured() {
      _ptrFifoShm->print();
    }

    void halted() {
      _ptrFifoShm->print();
    }
  
    void running() {
      continueRun=true;
      signal(SIGINT,StandaloneRunWriterSignalHandler);

      const Record *rrr;
      //rrr->reset(FsmState::Running);
      
      //std::cout << "Running " << _eventNumber << std::endl;
      if(continueRun) {
	if(_ptrFifoShm->readable()==0) {
	  if(_dummyReader) _ptrFifoShm->writeIncrement();
	  usleep(10);
	} else {
	  rrr=_ptrFifoShm->readRecord();
	  //rrr->print();
	  _ptrFifoShm->readIncrement();
	  if(_dummyReader) _ptrFifoShm->writeIncrement();
	  _fileWriter.write(rrr);
	  _eventNumber++;
	}
      }
      
      signal(SIGINT,SIG_DFL);
    }
   
  private:
    ShmSingleton<RunWriterDataFifo> shmU;
    RunWriterDataFifo *_ptrFifoShm;

    FileWriter _fileWriter;
    unsigned _linkNumber;
    unsigned _eventNumber;
    unsigned _runNumber;
    unsigned _relayNumber;

    bool _printEnable;
    bool _checkEnable;
    bool _assertEnable;
    bool _dummyReader;
  };

}

#endif
