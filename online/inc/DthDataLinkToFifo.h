#include <bits/stdc++.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fstream>
#include <iostream>


#include "DthHeader.h"
#include "SlinkBoe.h"
#include "SlinkEoe.h"


#include "SystemParameters.h"
#include "ShmSingleton.h"
#include "DataFifo.h"
#include "Record.h"

using namespace Hgcal10gLinkReceiver;

#define MAXLINE 4096

//#define PORT 10000



#define BUFFER_SIZE (1024*1024)             /* 1 MB */
int BUFFER_SIZE_MAX=BUFFER_SIZE-1024;                                    /* What buffer size to use? Up to 1MB */

unsigned n64Tcp(0),i64Tcp(0);
uint64_t *buffer=new uint64_t[BUFFER_SIZE/8];

bool printEnable(false);
int connected(0);

void getTcpPacket() {
  int n(0);

  while(n<8) { // Catch no packet
    n = recv(connected, buffer, BUFFER_SIZE_MAX, 0);
    
    if(n<8 || (n%8)!=0) std::cerr << "WARNING: number of bytes read = " << n << " gives n%8 = " << (n%8)
				  << " != 0" << std::endl;
    if(n>=BUFFER_SIZE) std::cerr << "WARNING: number of bytes read = " << n << " >= "
				 << BUFFER_SIZE << std::endl;
    
    // Initialise number of words in TCP packet
    n64Tcp=(n+7)/8;
    i64Tcp=0;
    
    if(printEnable) {
      std::cout  << std::endl << "************ GOT DATA ******************" << std::endl << std::endl;
      
      std::cout << "TCP size = " << n << " bytes = " << n64Tcp << " uint64_t words" << std::endl;
      
      // Short or long printout of buffer
      if(true) {
	if(n>= 8) std::cout << "First word        = " << std::hex << std::setw(16) << buffer[0] << std::dec << std::endl;
	if(n>=16) std::cout << "Second word       = " << std::hex << std::setw(16) << buffer[1] << std::dec << std::endl;
	if(n>=24) std::cout << "Third word        = " << std::hex << std::setw(16) << buffer[2] << std::dec << std::endl;
	if(n>=32) std::cout << "Fourth word       = " << std::hex << std::setw(16) << buffer[3] << std::dec << std::endl;
	if(n>=24) std::cout << "Last-but-one word = " << std::hex << std::setw(16) << buffer[n64Tcp-2] << std::dec << std::endl;
	if(n>=32) std::cout << "Last word         = " << std::hex << std::setw(16) << buffer[n64Tcp-1] << std::dec << std::endl;
	
      } else {
	for(unsigned i(0);i<n64Tcp;i++) {
	  std::cout << "Word " << std::setw(6) << i << " = " << std::hex << std::setw(16) << buffer[i] << std::dec << std::endl;
	}
      }
    }
  }
}


bool DthDataLinkToFifo(uint32_t key, uint16_t port, bool w=false, bool p=false) {
  std::cout << "DthDataLinkToFifo called with key = 0x"
            << std::hex << std::setfill('0')
            << std::setw(8) << key << ", port = 0x"
            << std::setw(4) << port
            << std::dec << std::setfill(' ')
            << std::endl;

  ShmSingleton<RunWriterDataFifo> shmU;
  RunWriterDataFifo *ptrRunFileShm=shmU.setup(key);
  ptrRunFileShm->coldStart();
  
  // Define control flags
  bool dummyWriter(w);

  printEnable=p;
  bool checkEnable(true);
  bool assertEnable(true);
   
  //setMemoryBuffer(ptrRunFileShm);

  int sock;
    int istrue = 1;          
  struct sockaddr_in server_addr, client_addr;    
  unsigned int sin_size;

  //int i, closed, used;
        
  //int client_count = 0;
  //pthread_t thread;       
  //cpu_set_t cpuset;
  //int cpu;

  //char client_description[100];

  if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
    perror("socket");
    exit(1);
  }
							
  /* set SO_REUSEADDR on a socket to bypass TIME_WAIT state */

  if(false) {
  if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &istrue, sizeof(int)) == -1) {
    perror("setsockopt");
    exit(1);
  }
  }

  //if (options.net_if) {
    /* Bind socket to a device */
  /*
    if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, options.net_if, strlen(options.net_if)) == -1) {
      perror("setsockopt");
      exit(1);
    }
  }
  */    
        
  server_addr.sin_family = AF_INET;         
  server_addr.sin_port = htons(port);     
  server_addr.sin_addr.s_addr = INADDR_ANY; 
  bzero(&(server_addr.sin_zero), 8); 

  if (bind(sock, (struct sockaddr *)&server_addr, sizeof(struct sockaddr)) == -1) {
    perror("Unable to bind");
    exit(1);
  }

  if (listen(sock, 1) == -1) {
    perror("Listen");
    exit(1);
  }
                        
  printf("\nListening on port %u\n", port);
  /*
  memset(&clients, 0, sizeof(clients));
  if (options.cpu != -1) {
    cpu = options.cpu;
  } else {
    cpu = 1;
  }
  */

  sin_size = sizeof(struct sockaddr_in);
  connected = accept(sock, (struct sockaddr *)&client_addr,&sin_size);

  printf("\nConnected...\n");


  RecordT<8*1024-1> *rt=new RecordT<8*1024-1>;
  uint64_t *ptrt((uint64_t*)rt);
  
  Record *r;

  unsigned n64Packet(0),i64Packet(0);
  unsigned nBlocks;


 
  Hgcal10gLinkReceiver::DthHeader dthh;
  uint64_t *dthw((uint64_t*)(&dthh));

  //bool newEvent(true);
  uint32_t seqc(0);
  unsigned nWords=0;

  // TCP packet loop
  bool continueLoop(true);
  while(continueLoop) {
    if(printEnable) std::cout << "Start of continue loop: i64Packet = " << i64Packet << ", n64Packet = " << n64Packet << std::endl;

    // DTH header for next block
    if(assertEnable) assert(n64Tcp>=i64Tcp);
    if(i64Tcp>=n64Tcp) getTcpPacket();
    dthw[0]=buffer[i64Tcp];
    i64Tcp++;
    
    if(assertEnable) assert(n64Tcp>=i64Tcp);
    if(i64Tcp>=n64Tcp) getTcpPacket();
    dthw[1]=buffer[i64Tcp];
    i64Tcp++;
    
    //dthh=*((Hgcal10gLinkReceiver::DthHeader*)(buffer+i64Tcp));
    //i64Tcp+=2;
    
    if(printEnable) dthh.print();
    
    // Start of fragment (= event) so initialise record
    if(dthh.blockStart()) {
      rt->reset(FsmState::Running);
      rt->setSequenceCounter(seqc++);
      if(printEnable) rt->RecordHeader::print();
      nWords=0;
      nBlocks=0;
    }
    
    // Check DTH header
    if(checkEnable) {
      bool dthHeaderError(false);
      
      if(!dthh.valid()) {
	std::cerr << "WARNING: DthHeader not valid" << std::endl;
	dthHeaderError=true;
      }
      
      if(dthh.blockStart()!=(nBlocks==0)) {
	std::cerr << "WARNING: DthHeader blockStart not valid; nBlocks = " <<  nBlocks << std::endl;
	dthHeaderError=true;
      }
      
      if(dthh.blockNumber()!=nBlocks) {
	std::cerr << "WARNING: DthHeader blockNumber not valid; nBlocks = " <<  nBlocks << std::endl;
	dthHeaderError=true;
      }
      
      if(dthHeaderError) dthh.print(std::cerr);
    }
    
    // Initialise for next block
    n64Packet=2*dthh.length();
    nBlocks++;	
    
    if(printEnable) std::cout << "Start of payload copy: i64Packet = " << i64Packet << ", n64Packet = " << n64Packet
			      << ", i64Tcp = " << i64Tcp << ", n64Tcp = " << n64Tcp << std::endl;

    // Find number of words for this fragment and copy to local record
    while(i64Tcp+n64Packet>n64Tcp) {
      if(assertEnable) assert(n64Tcp>=i64Tcp);
      unsigned nCpy(n64Tcp-i64Tcp);
      
      std::memcpy(ptrt+1+nWords,buffer+i64Tcp,8*nCpy);
      i64Tcp+=nCpy;
      nWords+=nCpy;
      n64Packet-=nCpy;
      
      getTcpPacket();
    }
    
    std::memcpy(ptrt+1+nWords,buffer+i64Tcp,8*n64Packet);
    i64Tcp+=n64Packet;
    nWords+=n64Packet;
    
    /*
      unsigned words64=std::min(n64Packet-i64Packet,n64Tcp-i64Tcp);
      if(printEnable) {
      std::cout << "Packet words = min(" << n64Packet-i64Packet << ", " << n64Tcp-i64Tcp
      << ") = " << words64 << " uint64_t words" << std::endl;
      } 
      
      std::memcpy(ptrt+1+nWords,buffer+i64Tcp,8*words64);
      nWords+=words64;
      i64Tcp+=words64;
      i64Packet+=words64;
    */
    
    // End of fragment (= event) so copy local record to FIFO
    //if(i64Packet>=n64Packet && dthh.blockStop()) {
    if(dthh.blockStop()) {
      if(printEnable) std::cout << "Fragment end: i64Packet = " << i64Packet << ", n64Packet = " << n64Packet << std::endl;
      if(nWords>=8*1024) std::cerr << "WARNING: number of packet words = " << nWords << " >= "
				   << 8*1024 << std::endl;
      
      rt->setPayloadLength(nWords);
      if(printEnable) rt->print();
      
      //n64Packet=0;
      
      // Wait until space available in FIFO
      while((r=(Record*)(ptrRunFileShm->getWriteRecord()))==nullptr) usleep(10);
      
      if(printEnable) std::cout << std::endl << "************ GOT MEMORY ******************" << std::endl << std::endl;
      
      // Copy to FIFO and update write pointer
      std::memcpy(r,rt,rt->totalLengthInBytes());	
      ptrRunFileShm->writeIncrement();
      
      if(dummyWriter) ptrRunFileShm->readIncrement();
      if(printEnable) ptrRunFileShm->print();
    }    
  } // End of continue loop

  delete rt;
  
  return true;
}
