#ifndef Hgcal10gLinkReceiver_MvChecker_h
#define Hgcal10gLinkReceiver_MvChecker_h

#include <iostream>
#include <iomanip>
#include <cstdint>
#include <cstring>


namespace Hgcal10gLinkReceiver {

  class MvChecker {

  public:
    MvChecker() {
    }

    void initializing() {
    }

    void configuring() {
    }

    void reconfiguring() {
    }

    void starting() {
    }

    void pausing() {
    }

    void resuming() {
    }

    void stopping() {
    }

    void halting() {
    }

    void resetting() {
    }

    void ending() {
    }

    //////////////////////////////////////////////

    // Return false to write out event to bin file

    // I.e. if check fails, return false to save event and
    // allow offline checking

    bool check(const Record *r) {

      const Hgcal10gLinkReceiver::RecordRunning  *rEvent((Hgcal10gLinkReceiver::RecordRunning*) r);
      const uint64_t *p64(((const uint64_t*)rEvent)+1);
      uint8_t counter = 0;
      std::vector<std::pair<int, int>> ranges = {
        {5, 13}, {15, 22}, {25, 32}, {34, 42}, {44, 52},
        {54, 61}, {64, 71}, {73, 81}, {83, 91}, {93, 100},
        {103, 110}, {112, 120}
      };

      // Performing the check over the given event
      if (rEvent->payloadLength() != 124) {
	// event is not of expected size
	// no need for further checking, it should be saved
        return false;
      }
      else {
        // event is of the expected size
	// checking individual words for corruption	      
        for (const auto& range : ranges) {
          for (int i = range.first; i <= range.second; ++i) {
            if (p64[i] != 0x2aaaa8002aaaa800) {
              counter++; // Increment the counter for mismatches
            }
          }
        }
	// save event if there were issues found
        return (counter == 0);
      }
    }

  private:

  };

}

#endif






// // Paul's original template
// 
// #ifndef Hgcal10gLinkReceiver_MvChecker_h
// #define Hgcal10gLinkReceiver_MvChecker_h
// 
// #include <iostream>
// #include <iomanip>
// #include <cstdint>
// #include <cstring>
// 
// 
// namespace Hgcal10gLinkReceiver {
// 
//   class MvChecker {
// 
//   public:
//     MvChecker() {
//     }
// 
//     void initializing() {
//     }
//     
//     void configuring() {
//     }
//     
//     void reconfiguring() {
//     }
//     
//     void starting() {
//     }
//     
//     void pausing() {
//     }
//     
//     void resuming() {
//     }
//     
//     void stopping() {
//     }
//     
//     void halting() {
//     }
//     
//     void resetting() {
//     }
// 
//     void ending() {
//     }
// 
//     //////////////////////////////////////////////
// 
//     // Return false to write out event to bin file
// 
//     // I.e. if check fails, return false to save event and
//     // allow offline checking
//     
//     bool check(const Record *r) {
//       return true;
//     }
//    
//   private:
// 
//   };
// 
// }
// 
// #endif
