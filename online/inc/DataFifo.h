
#ifndef Hgcal10gLinkReceiver_DataFifo_h
#define Hgcal10gLinkReceiver_DataFifo_h

#include <iostream>
#include <iomanip>

#include "Record.h"

namespace Hgcal10gLinkReceiver {

template<unsigned PowerOfTwo, unsigned Width> class DataFifoT {
public:
  enum {
    BufferDepthPowerOfTwo=PowerOfTwo,
    BufferDepth=(1<<BufferDepthPowerOfTwo),
    BufferDepthMask=BufferDepth-1,
    BufferWidth=Width
  };

  DataFifoT() {
  }

  // To be called by reading processors
  void coldStart() {
    _writePtr=0;
    _readPtr=0;
  }

  void setPayloadLengths(unsigned l) {
    if(l>=BufferWidth) l=BufferWidth-1;
    for(unsigned i(0);i<BufferDepth;i++) {
      _buffer[i].reset(FsmState::Running);
      _buffer[i].setPayloadLength(l);
      _buffer[i].setSequenceCounter(i);
      if(i==5) _buffer[i].RecordHeader::print();
    }
  }

  void setMaxPayloadLengths() {
    setPayloadLengths(BufferWidth-1);
  }

  void flush() {
    uint32_t wpOld(_writePtr);
    _readPtr=0xf0000000;
    usleep(1000);
    while(_writePtr>wpOld) {
      print();
      wpOld=_writePtr;
      usleep(1000);
    }
    _writePtr=0xffffffff;
    _readPtr=0xffffffff;
  }

  void initialize() {
    //_backPressure=false;
    _writePtr=0;
    _readPtr=0;
    _writeBytes=0;
    _readBytes=0;
  }
  
  void starting() {
    memset(this,0,sizeof(*this));
    //_backPressure=false;
    _writePtr=0;
    _readPtr=0;
    _writeBytes=0;
    _readBytes=0;
  }

  void end() {
    _writePtr=0xffffffff;
    _readPtr=0xffffffff;
    _writeBytes=0;
    _readBytes=0;
  }

  bool isEnded() {
    return _writePtr==0xffffffff && _readPtr==0xffffffff;
  }
  
  bool isEmpty() {
    return _writePtr==0 && _readPtr==0;
  }
  
  // Processor writing into FIFO

  uint32_t writeable() const {
    return _readPtr+BufferDepth-_writePtr;
  }

  Record* getWriteRecord() {
    if(writeable()==0) return nullptr;
    //return (Record*)_buffer[_writePtr&BufferDepthMask];
    return &(_buffer[_writePtr&BufferDepthMask]);
  }

  void writeIncrement() {
    _writeBytes+=8*getWriteRecord()->payloadLength();
    _writePtr++;
  }
  
  // Reading from FIFO to disk or ZMQ

  uint32_t readable() {
    return _writePtr-_readPtr;
  }

  const Record* readRecord() {
    if(readable()==0) return nullptr;
    //return (const Record*)_buffer[_readPtr&BufferDepthMask];
    return &(_buffer[_readPtr&BufferDepthMask]);
  }

  Record* getReadRecord() {
    if(readable()==0) return nullptr;
    //return (Record*)_buffer[_readPtr&BufferDepthMask];
    return &(_buffer[_readPtr&BufferDepthMask]);
  }

  void readIncrement() {
    _readBytes+=8*getReadRecord()->payloadLength();
    _readPtr++;
  }
  

  // REDUNDANT?
  /*  
  bool writeRecord(const Record *r) {
    return write(r->payloadLength(),(const uint64_t*)r);
  }

  bool write(uint16_t n, const uint64_t *p) {
    if(_writePtr==_readPtr+BufferDepth) return false;
    //std::memcpy(_buffer[_writePtr%BufferDepth],p,8*n);
    std::memcpy(_buffer[_writePtr&BufferDepthMask],p,8*n);
    _writePtr++;
    return true;
  }

  uint16_t read(uint64_t *p) {
    if(_writePtr==_readPtr) return 0;
    RecordHeader *h((RecordHeader*)_buffer[_readPtr&BufferDepthMask]);
    uint16_t n(h->totalLength());
    std::memcpy(p,h,8*n);
    _readPtr++;
    return n;
  }
  */

  // Only if applying back pressure
  
  uint32_t writeableQuarters() const {
    return (4*writeable())/BufferDepth;
  }
  /*
  bool backPressure() const {
    return _backPressure;
  }

  void setBackPressure(bool b) {
    _backPressure=b;
  }
  */
  uint32_t writePtr() const {
    return _writePtr;
  }
  
  uint64_t writeBytes() const {
    return _writeBytes;
  }
  
  void print(std::ostream &o=std::cout) {
    uint32_t rp(_readPtr),wp(_writePtr);
    uint64_t rb(_readBytes),wb(_writeBytes);
      
    uint32_t dp(wp>rp?wp-rp:0);
    uint64_t db(wb>rb?wb-rb:0);

    o << "DataFifoT<" << PowerOfTwo << "," << Width << ">::print()" << std::endl;
    o << " Write pointer to memory  = " << std::setw(10) << wp
      << ",  bytes written    = " << std::setw(20) << wb << std::endl;
    o << " Read pointer from memory = " << std::setw(10) << rp
      << ",  bytes read       = " << std::setw(20) << rb << std::endl;
    o << " Difference               = " << std::setw(10) << dp
      << ",  bytes difference = " << std::setw(20) << db << std::endl;
    //o << " Back pressure = " << (_backPressure?"on":"off") << std::endl; OBSOLETE
  }

  //bool _backPressure;
  uint32_t _writePtr;
  uint32_t _readPtr;
  uint64_t _writeBytes;
  uint64_t _readBytes;
  //uint64_t _buffer[BufferDepth][BufferWidth];
  RecordT<BufferWidth-1> _buffer[BufferDepth];
};

 typedef DataFifoT<4,16*1024> RelayWriterDataFifo;
 typedef RecordT<16*1024-1>   RelayWriterDataFifoRecord;

 typedef DataFifoT<13,2*1024>    RunWriterDataFifo;
 typedef RecordT<2*1024-1>      RunWriterDataFifoRecord;
 
}
#endif
