#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <fstream>
#include <iostream>
#include <cassert>

//#define ProcessorHardware

#include "SerenityHgcroc.h"
#include "SerenityMiniDaq.h"
#include "SerenityTcds2.h"
#include "ProcessorFastControlPlusDaq.h"

using namespace Hgcal10gLinkReceiver;

int main(int argc, char *argv[]) {

  // Turn off printing
  bool enable(false);

  for(int i(1);i<argc;i++) {
    if(std::string(argv[i])=="-en") {
      //std::cout << "Setting enable to true" << std::endl;
      enable=true;
    }
  }

  SerenityUhal::setUhalLogLevel();

  SerenityTcds2 smd;
  smd.makeTable();

  if(enable) {
    // Enable active HGCROC throttle
    smd.uhalWrite("ctrl_stat.ctrl2.tts_mask",0x3);
  } else {
    // Disable HGCROC throttle
    smd.uhalWrite("ctrl_stat.ctrl2.tts_mask",0x1);
  }

  //std::cout << "ctrl_stat.ctrl2.tts_mask set to " << smd.uhalRead("ctrl_stat.ctrl2.tts_mask") << std::endl;
  return 0;
}
