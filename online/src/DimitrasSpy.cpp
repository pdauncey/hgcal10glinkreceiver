#include <string>
#include <chrono>
#include <thread>
#include <iostream>
#include <fstream>
#include <cstring>
#include <unistd.h>

#include "SystemParameters.h"
#include "DataFifo.h"
#include "ShmSingleton.h"

using namespace Hgcal10gLinkReceiver;

uint64_t timeSinceEpochMillisec() {
  using namespace std::chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

int main() {

  ShmSingleton<RunWriterDataFifo> shmD[3];
  RunWriterDataFifo *ptrD[3];

  //ShmSingleton<RelayWriterDataFifo> shmC[3];
  //RelayWriterDataFifo *ptrC[3];
  
  ptrD[0]=shmD[0].setup(ProcessorFastControlDl0FifoShmKey);
  ptrD[1]=shmD[1].setup(ProcessorFastControlDl1FifoShmKey);
  ptrD[2]=shmD[2].setup(ProcessorFastControlDl2FifoShmKey);
  /*
  ptrC[0]=shmC[0].setup(ProcessorFastControlCl0FifoShmKey);
  ptrC[1]=shmC[1].setup(ProcessorTcds2Cl1FifoShmKey);
  ptrC[2]=shmC[2].setup(ProcessorFrontEndCl2FifoShmKey);

  uint32_t w[3],wOld[3]={0,0,0},wDiff[3];
  uint64_t t[2];
  t[0]=0;
  
  std::cout << std::setprecision(3) << std::fixed;
  */
  std::ofstream fout;
  while(true) {
    fout.open("DimitrasSpy.txt");
    for(unsigned i(0);i<3;i++) {
      fout << ptrD[i]->_readPtr;
      if(i<2) fout << ",";
    }
    fout << std::endl;
    fout.close();
    usleep(1000000);
  }
  
  return 0;
}
