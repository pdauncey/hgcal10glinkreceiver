#include <string>
#include <chrono>
#include <thread>
#include <iostream>
#include <cstring>
#include <unistd.h>

#include "SystemParameters.h"
#include "DataFifo.h"
#include "ShmSingleton.h"

using namespace Hgcal10gLinkReceiver;

uint64_t timeSinceEpochMillisec() {
  using namespace std::chrono;
  return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}

int main() {
  ShmSingleton<RunWriterDataFifo> shmD[3];
  RunWriterDataFifo *ptrD[3];

#ifdef SHOW_CONFIGURATION_LINKS
  ShmSingleton<RelayWriterDataFifo> shmC[3];
  RelayWriterDataFifo *ptrC[3];
#endif
  
  ptrD[0]=shmD[0].setup(ProcessorFastControlDl0FifoShmKey);
  ptrD[1]=shmD[1].setup(ProcessorFastControlDl1FifoShmKey);
  ptrD[2]=shmD[2].setup(ProcessorFastControlDl2FifoShmKey);

#ifdef SHOW_CONFIGURATION_LINKS
  ptrC[0]=shmC[0].setup(ProcessorFastControlCl0FifoShmKey);
  ptrC[1]=shmC[1].setup(ProcessorTcds2Cl1FifoShmKey);
  ptrC[2]=shmC[2].setup(ProcessorFrontEndCl2FifoShmKey);
#endif

  uint32_t w[3],wOld[3]={0,0,0},wDiff[3];
  uint64_t wb[3],wbOld[3]={0,0,0},wbDiff[3];
  uint64_t t[2];
  t[0]=0;
  
  std::cout << std::setprecision(3) << std::fixed;


while(true) {
    system("clear");

    t[1]=timeSinceEpochMillisec();
    double dt(t[1]-t[0]);
    
    for(unsigned i(0);i<3;i++) {
      ptrD[i]->print();

      w[i]=ptrD[i]->writePtr();
      wDiff[i]=(w[i]>=wOld[i]?w[i]-wOld[i]:0);

      wb[i]=ptrD[i]->writeBytes();
      wbDiff[i]=(wb[i]>=wbOld[i]?wb[i]-wbOld[i]:0);
    }
    
    std::cout << std::endl;
    for(unsigned i(0);i<3;i++) {
      std::cout << "Rates: Link " << i << " = " << std::setw(8) << wDiff[i]/dt
		<< " kHz,  event size = "
		<< std::setw(6) << (wDiff[i]==0?0:0.001*wbDiff[i]/wDiff[i]) 
		<< " kBytes,  data rate = " << std::setw(6) << 0.000008*wbDiff[i]/dt
		<< " Gbit/s" << std::endl;

      wOld[i]=w[i];
      wbOld[i]=wb[i];
    }
    //<< "       Link 1 = " << std::setw(8) << wDiff[1]/dt << " kHz" << std::endl
    //<< "       Link 2 = " << std::setw(8) << wDiff[2]/dt << " kHz" << std::endl << std::endl;

    t[0]=t[1];
    //wOld[0]=w[0];
    // wOld[1]=w[1];
    //wbOld[1]=wb[1];
    //wbOld[2]=wb[2];

#ifdef SHOW_CONFIGURATION_LINKS
    for(unsigned i(0);i<3;i++) {
      ptrC[i]->print();
    }
#endif
    
    std::cout << std::endl;
    system("df -h dat");
    usleep(1000000);
  }
  
  return 0;
}
