#ifndef Hgcal10gLinkReceiver_Stage12DataWord_h
#define Hgcal10gLinkReceiver_Stage12DataWord_h

/*
  [ 5: 0] : 8b TC0 channel
  [14: 6] : 9b TC0 energy
  [20:15] : 8b TC1 channel
  [14:21] : 9b TC1 energy
  [35:30] : 8b TC2 channel
  [44:36] : 9b TC2 energy
  [52:45] : 8b Tower0 energy
  [60:53] : 8b Tower1 energy
  [63:61] : 3b Header pattern (0xa)
*/

#include <iostream>
#include <iomanip>
#include <cstdint>


namespace Hgcal10gLinkReceiver {

  class Stage12DataWord {
  
  public:
    Stage12DataWord() {
      reset();
    }
    
    void reset() {
      _data=0;
    }

    uint8_t pattern() const {
      return _data>>61;
    }

    uint16_t towerEnergy(unsigned t) const {
      assert(t<2);
      return (_data>>(45+8*t))&0xff;
    }
    
    uint8_t tcChannel(unsigned t) const {
      assert(t<3);
      //return (_data>>(15*t))&0x3f;
      return (_data>>(9+15*t))&0x3f; // TEMP
    }
    
    uint16_t tcEnergy(unsigned t) const {
      assert(t<3);
      //return (_data>>(6+15*t))&0x1ff;
      return (_data>>(15*t))&0x1ff; // TEMP
    }
    
    void print(std::ostream &o=std::cout, std::string s="") const {
      o << s << "Stage12DataWord::print()  Data = 0x"
        << std::hex << std::setfill('0')
        << std::setw(16) << _data
        << std::dec << std::setfill(' ')
	<< std::endl;
      o << s << " Pattern = 0x"
        << std::hex << std::setfill('0')
        << std::setw(1) << unsigned(pattern())
        << std::dec << std::setfill(' ')
	<< std::endl;
      o << s << " Tower energies = "
	<< std::setw(3) << towerEnergy(0) << ", "
	<< std::setw(3) << towerEnergy(1)
	<< std::endl;
      o << s << " TC energies/channels = "
	<< std::setw(3) << tcEnergy(0) << "/"
	<< std::setw(2) << unsigned(tcChannel(0)) << ", "
	<< std::setw(3) << tcEnergy(1) << "/"
	<< std::setw(2) << unsigned(tcChannel(1)) << ", "
	<< std::setw(3) << tcEnergy(2) << "/"
	<< std::setw(2) << unsigned(tcChannel(2))
	<< std::endl;
    }
    
  private:
    uint64_t _data;
  };

}

#endif
