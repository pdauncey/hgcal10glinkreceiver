#ifndef Hgcal10gLinkReceiver_OrbitHeader_h
#define Hgcal10gLinkReceiver_OrbitHeader_h

#include <iostream>
#include <iomanip>
#include <cstdint>

namespace Hgcal10gLinkReceiver {

class OrbitHeader {
 public:
  enum {
    OrbitHeaderPattern=0x4f48
  };

  OrbitHeader() {
    reset();
  }

  void reset() {
    _word[0]=OrbitHeaderPattern;
    _word[1]=0;
    _word[2]=0;
    _word[3]=0;
  }
  
  uint16_t  orbitHeaderPattern() const {
    return _word[0]&0xffff;
  }

  bool validPattern() const {
    return orbitHeaderPattern()==OrbitHeaderPattern;
  }
  
  uint16_t  version() const {
    return (_word[0]>>16)&0xffff;
  }
  
  uint32_t sourceId() const{
    return _word[0]>>32;
  }

  uint32_t runNumber() const {
    return _word[1]&0xffffffff;
  }
  
  uint32_t orbitNumber() const{
    return _word[1]>>32;
  }

  uint16_t  eventCount() const{
    return _word[2]&0xfff;
  }

  uint32_t packetWordCount() const{
    return _word[2]>>32;
  }

  uint32_t  flags() const{
    return _word[3]&0xffffffff;
  }

  uint32_t checkSum() const{
    return _word[3]>>32;
  }

  
  void setVersion(uint16_t v) {
    _word[0]&=0xffffffff0000ffff;
    _word[0]|=uint64_t(v)<<16;
  }

  void setSourceId(uint32_t s) {
    _word[0]&=0x00000000ffffffff;
    _word[0]|=uint64_t(s)<<32;
  }

  void setRunNumber(uint32_t n) {
    _word[1]&=0xffffffff00000000;
    _word[1]|=n;
  }

  void setOrbitNumber(uint32_t n) {
    _word[1]&=0x00000000ffffffff;
    _word[1]|=uint64_t(n)<<32;
  }

  void setEventCount(uint16_t e) {
    assert(e<0x1000);
    _word[2]&=0xfffffffffffff000;
    _word[2]|=e;
  }
  
  void setPacketWordCount(uint32_t p) {
    _word[2]&=0x00000000ffffffff;
    _word[2]|=uint64_t(p)<<32;
  }

  void setFlags(uint32_t f) {
    _word[3]&=0xffffffff00000000;
    _word[3]|=f;
  }

  void setCheckSum(uint32_t s) {
    _word[3]&=0x00000000ffffffff;
    _word[3]|=uint64_t(s)<<32;
  }
  
  bool valid() const {
    return orbitHeaderPattern()==OrbitHeaderPattern);
  }

  void print(std::ostream &o=std::cout, const std::string &s="") const {
    o << s << "OrbitHeader::print()  words = 0x"
      << std::hex << std::setfill('0')
      << std::setw(16) << _word[3] << ", 0x"
      << std::setw(16) << _word[2] << ", 0x"
      << std::setw(16) << _word[1] << ", 0x"
      << std::setw(16) << _word[0]
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Orbit header patterm = 0x"
      << std::hex << std::setfill('0')
      << std::setw(4) << unsigned(boeHeader())
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Version = " << std::setw(5) << unsigned(version())
      << std::endl;
    o << s << " Source id = 0x"
      << std::hex << std::setfill('0')
      << std::setw(8) << sourceId()
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Run number = " << std::setw(10) << runNumber()
      << std::endl;
    o << s << " Orbit number = " << std::setw(10) << orbitNumber()
      << std::endl;
    o << s << " Event count = " << std::setw(4) << eventCount()
      << std::endl;
    o << s << " Reserved = 0x"
      << std::hex << std::setfill('0')
      << std::setw(5) << (_word[2]>>12)&0xfffff
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Packet word count = " << std::setw(4) << packetWordCount()
      << " 128-bit words" << std::endl;
    o << s << " Flags = 0x"
      << std::hex << std::setfill('0')
      << std::setw(8) << flags()
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Check sum = 0x"
      << std::hex << std::setfill('0')
      << std::setw(8) << checkSum()
      << std::dec << std::setfill(' ')
      << std::endl;
  }

 private:
  uint64_t _word[4];
};

}

#endif
