#ifndef Hgcal10gLinkReceiver_RecordStopping_h
#define Hgcal10gLinkReceiver_RecordStopping_h

#include <iostream>
#include <iomanip>
#include <cassert>

#include "RecordYaml.h"

namespace Hgcal10gLinkReceiver {

  class RecordStopping : public RecordYaml {
  
  public:
    RecordStopping() {
    }
 
    void setHeader(uint32_t t=time(0)) {
      reset(FsmState::Stopping);
      setUtc(t);
      setPayloadLength(2);
    }

    bool valid() const {
      return validPattern() && state()==FsmState::Halting;
    }

    void print(std::ostream &o=std::cout, std::string s="") const {
      o << s << "RecordStopping::print()" << std::endl;
      RecordYaml::print(o,s+" ");

      if(false) {
      for(unsigned i(0);i<payloadLength();i++) {
	o << s << "   Payload word " << std::setw(5) << " = 0x"
	  << std::hex << std::setfill('0')
	  << std::setw(16) << _payload[i]
	  << std::dec << std::setfill(' ') << std::endl;
      }
      }
      /*
      o << s << " Relay number             = "
      << std::setw(10) << relayNumber() << std::endl;
      o << s << " Number of configurations = "
	<< std::setw(10) << numberOfConfigurations() << std::endl;
      o << s << " Number of runs           = "
	<< std::setw(10) << numberOfRuns() << std::endl;
      o << s << " Number of events         = "
	<< std::setw(10) << numberOfEvents() << std::endl;
    */
	}
    
  private:
  };

}

#endif
