#ifndef Hgcal10gLinkReceiver_FragmentTrailer_h
#define Hgcal10gLinkReceiver_FragmentTrailer_h

#include <iostream>
#include <iomanip>
#include <cstdint>

namespace Hgcal10gLinkReceiver {


class FragmentTrailer {
 public:
  enum {
    FragmentTrailerPattern=0x4654
  };
  
  FragmentTrailer() {
    reset();
  }

  void reset() {
    _word[0]=FragmentTrailerPattern;
    _word[1]=0;
  }

  uint16_t  fragmentTrailerPattern() const {
    return _word[0]&0xffff;
  }

  bool validPattern() const {
    return fragmentTrailerPattern()==FragmentTrailerPattern;
  }
  
  uint16_t flags() const {
    return (_word[0]>>16)&0xffff;
  }

  uint32_t fragmentSize() const {
    return _word[1]>>32;
  }

  uint64_t eventId() const {
    return _word[1]&0xfffffffffff;
  }

  uint16_t checkSum() const {
    return _word[1]>>48;
  }

  
  void setFlags(uint16_t f) {
    _word[0]&=0xffffffff0000ffff;
    _word[0]|=(uint64_t(f)<<16);
  }

  void setFragmentSize(uint32_t s) {
    _word[0]&=0x00000000ffffffff;
    _word[0]|=(uint64_t(s)<<32);
  }

  void setEventId(uint64_t e) {
    assert(e<(1U<<44));
    _word[1]&=0xfffff00000000000;
    _word[1]|=e;
  }

  void setCheckSum(uint16_t c) {  
    _word[1]&=0x0000ffffffffffff;
    _word[1]|=uint64_t(c)<<48;
  }
  
  bool valid() const {
    return fragmentTrailerPattern()==FragmentTrailerPattern;
  }

  void print(std::ostream &o=std::cout, const std::string &s="") const {
    o << s << "FragmentTrailer::print()  words = 0x"
      << std::hex << std::setfill('0')
      << std::setw(16) << _word[1] << ", 0x"
      << std::setw(16) << _word[0]
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Fragment trailer pattern = 0x"
      << std::hex << std::setfill('0')
      << std::setw(4) << fragmentTrailerPattern()
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Flags = 0x"
      << std::hex << std::setfill('0')
      << std::setw(4) << flags()
      << std::dec << std::setfill(' ')
      << std::endl;
    o << s << " Fragment size = "
      << std::setw(10) << fragmentSize()
      << " 128-bit words"
      << std::endl;
    o << s << " Event id = " << std::setw(14) << eventId()
      << std::endl;
    o << s << " Check sum = 0x"
      << std::hex << std::setfill('0')
      << std::setw(4) << checkSum()
      << std::dec << std::setfill(' ')
      << std::endl;
  }

 private:
  uint64_t _word[2];
};

}

#endif
