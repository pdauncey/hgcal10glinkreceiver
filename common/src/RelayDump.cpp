#include <iostream>
#include <fstream>
#include <iomanip>
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <cassert>
#include <sys/types.h>
#include <getopt.h>

#include <yaml-cpp/yaml.h>

#include "FileReader2.h"
#include "RelayReader.h"
#include "RecordPrinter.h"
//#include "RecordYaml.h"

using namespace Hgcal10gLinkReceiver;

int main(int argc, char** argv) {
  if(argc<2) {
    std::cerr << argv[0] << ": no relay number specified" << std::endl;
    return 1;
  }

  unsigned relayNumber(0);
  
  // Handle relay number
  std::istringstream issRelay(argv[1]);
  issRelay >> relayNumber;

  std::string fileName(std::string("dat/Relay")+argv[1]+"/Relay"+argv[1]+".bin");
  
  //FsmState::State state(FsmState::Halted);
  //FsmState::State previousState(FsmState::EndOfStateEnum);

  //RecordT<4*1024-1> r;
  RecordYaml ry;
  Record &r((Record&)ry);
  
  uint32_t nRecord[FsmState::EndOfStateEnum+1];
  for(unsigned i(0);i<=FsmState::EndOfStateEnum;i++) nRecord[i]=0;
  
  FileReader relayReader;
  assert(relayReader.open(fileName));
  
  while(relayReader.read(&r)) {
    ry.print();
    std::cout << std::endl;
    
    if(r.state()<FsmState::EndOfStateEnum) nRecord[r.state()]++;
    else nRecord[FsmState::EndOfStateEnum]++;
  }

  relayReader.close();
  
  // Print out table
  std::cout << std::endl << "Number of records seen by state" << std::endl;
  for(unsigned i(0);i<=FsmState::EndOfStateEnum;i++) {
    std::cout << " State " << FsmState::stateName((FsmState::State)i)
	      << ", number of records = " << std::setw(10)
	      << nRecord[i] << std::endl;
  }

  //std::cout << std::setw(10) << relayNumber << ", "
  //	    << std::setw(10) << nRecord[FsmState::Running] << std::endl;
    
  return 0;
}
